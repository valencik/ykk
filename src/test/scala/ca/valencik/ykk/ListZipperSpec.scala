package ca.valencik.ykk

import org.scalatest._

class ListZipperSpec extends FlatSpec with Matchers {
  "ListZipper" should "accept a List and index for creation" in {
    ListZipper(List(1, 2, 3), 0) shouldBe ListZipper(List.empty, 1, List(2, 3))
    ListZipper(List(1, 2, 3), 1) shouldBe ListZipper(List(1), 2, List(3))
    ListZipper(List(1, 2, 3), 2) shouldBe ListZipper(List(2, 1), 3, List.empty)
  }

  "leftMaybe" should "move left when lefts is not empty" in {
    ListZipper(List(1), 2, List.empty).leftMaybe shouldBe Some(ListZipper(List.empty, 1, List(2)))
    ListZipper(List(3, 2, 1), 4, List(5, 6)).leftMaybe shouldBe Some(ListZipper(List(2, 1), 3, List(4, 5, 6)))
  }

  it should "return None when lefts is empty" in {
    ListZipper(List.empty, 0, List.empty).leftMaybe shouldBe None
    ListZipper(List(1), 2, List.empty).leftMaybe.get.leftMaybe shouldBe None
  }

  "left" should "move left when lefts is not empty" in {
    ListZipper(List(1), 2, List.empty).left shouldBe ListZipper(List.empty, 1, List(2))
    ListZipper(List(3, 2, 1), 4, List(5, 6)).left shouldBe ListZipper(List(2, 1), 3, List(4, 5, 6))
  }

  it should "return unchanged when lefts is empty" in {
    ListZipper(List.empty, 0, List.empty).left shouldBe ListZipper(List.empty, 0, List.empty)
    ListZipper(List(1), 2, List.empty).left.left shouldBe ListZipper(List.empty, 1, List(2))
  }

  "lHead" should "return the head of lefts when lefts is not empty" in {
    ListZipper(List(1), 2, List.empty).lHead shouldBe 1
    ListZipper(List(3, 2, 1), 4, List(5, 6)).lHead shouldBe 3
  }

  it should "return the focus when lefts is empty" in {
    ListZipper(List.empty, 0, List.empty).lHead shouldBe 0
    ListZipper(List(1), 2, List.empty).left.lHead shouldBe 1
  }

  "rightMaybe" should "move right when rights is not empty" in {
    ListZipper(List.empty, 1, List(2)).rightMaybe shouldBe Some(ListZipper(List(1), 2, List.empty))
    ListZipper(List(3, 2, 1), 4, List(5, 6)).rightMaybe shouldBe Some(ListZipper(List(4, 3, 2, 1), 5, List(6)))
  }

  it should "return None when rights is empty" in {
    ListZipper(List.empty, 0, List.empty).rightMaybe shouldBe None
    ListZipper(List.empty, 1, List(2)).rightMaybe.get.rightMaybe shouldBe None
  }

  "right" should "move right when rights is not empty" in {
    ListZipper(List.empty, 1, List(2)).right shouldBe ListZipper(List(1), 2, List.empty)
    ListZipper(List(3, 2, 1), 4, List(5, 6)).right shouldBe ListZipper(List(4, 3, 2, 1), 5, List(6))
  }

  it should "return None when rights is empty" in {
    ListZipper(List.empty, 0, List.empty).right shouldBe ListZipper(List.empty, 0, List.empty)
    ListZipper(List.empty, 1, List(2)).right.right shouldBe ListZipper(List(1), 2, List.empty)
  }

  "rHead" should "return the head of rights when rights is not empty" in {
    ListZipper(List.empty, 1, List(2)).rHead shouldBe 2
    ListZipper(List(3, 2, 1), 4, List(5, 6)).rHead shouldBe 5
  }

  it should "return the focus when rights is empty" in {
    ListZipper(List.empty, 0, List.empty).rHead shouldBe 0
    ListZipper(List.empty, 1, List(2)).right.rHead shouldBe 2
  }

  "toList" should "return a list of all elements" in {
    ListZipper(List(3, 2, 1), 4, List(5, 6)).toList shouldBe List(1, 2, 3, 4, 5, 6)
    ListZipper(List.empty, 1, List(2, 3)).toList shouldBe List(1, 2, 3)
    ListZipper(List(2, 1), 3, List.empty).toList shouldBe List(1, 2, 3)
    ListZipper(List.empty, 1, List.empty).toList shouldBe List(1)
  }

  "zipZip" should "return a ListZipper with elements zipped together" in {
    val lz123 = ListZipper(List(1), 2, List(3))
    lz123.zipZip(lz123) shouldBe ListZipper(List((1, 1)), (2, 2), List((3, 3)))
    ListZipper(List(3, 2, 1), 4, List(5, 6)).zipZip(lz123) shouldBe ListZipper(List((3, 1)), (4, 2), List((5, 3)))
    ListZipper(List.empty, 4, List(5, 6)).zipZip(lz123) shouldBe ListZipper(List.empty, (4, 2), List((5, 3)))
    ListZipper(List(3, 2, 1), 4, List.empty).zipZip(lz123) shouldBe ListZipper(List((3, 1)), (4, 2), List.empty)
  }

  "show" should "return a string with lefts and rights and the focus" in {
    import cats.implicits._
    ListZipper(List(3, 2, 1), 4, List(5, 6)).show shouldBe "|1,2,3|> 4 <|5,6|"
    ListZipper(List.empty, 4, List(5, 6)).show shouldBe "||> 4 <|5,6|"
    ListZipper(List(3, 2, 1), 4, List.empty).show shouldBe "|1,2,3|> 4 <||"
  }

  "map" should "return a ListZipper with the function applied to its elements" in {
    import cats.implicits._
    ListZipper(List(2, 1), 3, List(4)).map(identity) shouldBe ListZipper(List(2, 1), 3, List(4))
    ListZipper(List(2, 1), 3, List(4)).map(_ + 1) shouldBe ListZipper(List(3, 2), 4, List(5))
  }

  "extract" should "return the focus value" in {
    import cats.implicits._
    ListZipper(List(2, 1), 3, List(4)).extract shouldBe 3
    ListZipper(List.empty, 1, List.empty).extract shouldBe 1
  }
}
