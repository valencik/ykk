package ca.valencik.ykk

import cats.implicits._

object NumericZipper {
  def latch[A](z: ListZipper[A])(implicit ordering: Ordering[A]): A =
    if (z.lefts.isEmpty) z.focus else (z.focus :: z.lefts).max

  def peak[A](z: ListZipper[A])(implicit ordering: Ordering[A]): Boolean =
    ordering.lt(z.lHead, z.focus) && ordering.lt(z.rHead, z.focus)

  def wma(n: Int)(z: ListZipper[Double]): Double = {
    val window = z.lefts.take(n) ++ (z.focus :: z.rights.take(n))
    window.sum / window.size
  }
}

object ZippingNumbers extends App {
  import NumericZipper._
  def putStr(line: String): Unit = {
    // scalastyle:off
    println(line)
    // scalastyle:on
  }

  val iz: ListZipper[Int] = ListZipper(List(1, 2, 3, 2, 1, 3, 4, 5, 4, 2, 8), 0)

  val leftMaxes = iz.coflatMap[Int](latch)
  putStr(leftMaxes.show)

  val peaks = iz.coflatMap(peak(_))
  putStr(peaks.show)

  val averages = iz.map(_.toDouble).coflatMap(wma(5)).map { d =>
    f"$d%.3f"
  }
  putStr(averages.show)

  val ilz = iz.zipZip(leftMaxes).zipZip(peaks).zipZip(averages).map {
    case (((i, l), p), a) => (i, l, p, a)
  }
  putStr(ilz.map { case (i, l, p, a) => s"$i   $l   $p   $a" }.toList.mkString("\n"))
}
